#ifndef __AMSN_PARSER_H__
#define __AMSN_PARSER_H__

#include <glib.h>

typedef enum {
  EOS, /* End of stream/EOF */
  CHA, /* ChatMessage */
  ERR,

  RED, /* conv start/end */
  GRA, /* time (msg) */
  ITA, /* user */
  C00  /* message */
} RecordType;

typedef struct {
  gchar *message;
  gchar *code_suffix; /* e.g. 00ff (received) or 0000 (sent) */
} ChatMessageContent;

typedef struct {
  gchar *sender;
  gchar *dt;
  gchar *message;
} ChatMessage;

typedef struct {
  gchar *chat_name;
  GList *messages; /* ChatMessage */
} ChatLog;

typedef struct {
  gboolean quiet;
  gboolean json;
  gchar *cur_date;
  GList *logs;
  JsonBuilder *builder;
  GIOStream *_ios;
  GOutputStream *outs;
  GCancellable *can;
} ChatParser;

typedef union {
  gchar *s;
  ChatMessageContent c;
  ChatMessage *m;
} RecordData;

typedef struct {
  RecordType type;
  RecordData data;
} Record;

void chat_parser_print (ChatParser *parser);
gboolean chat_parser_parse_file (ChatParser *parser, char *fpath, GCancellable *can);

#endif /* __AMSN_PARSER_H_ */
