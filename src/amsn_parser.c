#define _XOPEN_SOURCE

#include <stdio.h>
#include <gio/gio.h>
#include <gio/gunixoutputstream.h>
#include <stdlib.h>
#include <time.h>
#include <json-glib/json-glib.h>
#include "amsn_parser.h"

#define BUFFER_LEN 4000
#define FIELD_SEP "|\"L"

static GRegex *g_filename_re      = NULL;
static GRegex *g_unix_time_re     = NULL;
static GRegex *g_dmy_time_re      = NULL;
static GRegex *g_hms_time_re      = NULL;
static GRegex *g_just_hms_time_re = NULL;
static GRegex *g_iso8061_dt_re    = NULL;

GIOStream *
get_iostream_for_path (char *out_path, GCancellable *can) {
  GError *err = NULL;
  GFile *out_file = NULL;
  GFileIOStream *ios = NULL;

  out_file = g_file_new_for_path (out_path);
  if (g_file_query_exists (out_file, can)) {
    fprintf (stderr, "ERROR: Specified output path '%s' already exists.  Refusing to overwrite.\n", out_path);
    exit (1);
  }

  ios = g_file_create_readwrite (out_file, G_FILE_CREATE_NONE, can, &err);
  if (err) {
    fprintf (stderr, "ERROR: %s\n", err->message);
    exit (1);
  }

  g_object_unref (out_file);

  return G_IO_STREAM (ios);
}

ChatMessage *
chat_message_new_take (gchar *sender, const gchar *dt, const gchar *time, gchar *message) {
  /* takes sender and message, but not dt or time */
  ChatMessage *this = NULL;
  GError *err = NULL;

  this = g_malloc0 (sizeof (ChatMessage));

  this->sender = sender;
  this->message = message;
  this->dt = NULL;

  if (g_regex_match (g_just_hms_time_re, time, 0, NULL)) {
    g_assert (dt != NULL);
    this->dt = g_regex_replace (g_hms_time_re, dt, -1, 0, time, G_REGEX_MATCH_DEFAULT, &err);
    if (err) {
      fprintf (stderr, "ERROR: chat_message_new_take: failed to calculate date time: %s\n", err->message);
      g_error_free (err);
    }
  } else if (g_regex_match (g_iso8061_dt_re, time, 0, NULL)) {
    this->dt = g_strdup (time);
  } else {
    this->dt = g_strdup_printf ("(%s) %s", dt, time);
  }

  return this;
}

ChatMessage *
chat_message_new (const gchar *sender, const gchar *dt, const gchar *time, const gchar *message) {
  return chat_message_new_take (g_strdup (sender),
                                dt,
                                time,
                                g_strdup (message));
}

void
chat_message_free (ChatMessage *this) {
  free (this->sender);
  free (this->message);
  free (this->dt);
  free (this);
}

void
chat_message_print (ChatMessage *this, GOutputStream *outs, GCancellable *can) {
  gsize _bytes_writ;
  GError *err = NULL;

  g_output_stream_printf (outs, &_bytes_writ, can, &err, "[%s] [%-20s]: [%s]\n", this->dt, this->sender, this->message);

  if (err) {
    fprintf (stderr, "ERROR: %s\n", err->message);
  }
}

ChatLog *
chat_log_new (const gchar *chat_name) {
  ChatLog *this = g_malloc0 (sizeof (ChatLog));

  this->chat_name = g_strdup (chat_name);
  this->messages = NULL;

  return this;
}

void
chat_log_free (ChatLog *this) {
  g_free (this->chat_name);
  g_list_free_full (this->messages, (GDestroyNotify)chat_message_free);
  g_free (this);
}

void
chat_parser_add_log_to_json_builder (ChatParser *parser, ChatLog *log) {
  json_builder_begin_object (parser->builder);

  json_builder_set_member_name (parser->builder, "name");
  json_builder_add_string_value (parser->builder, log->chat_name);

  json_builder_set_member_name (parser->builder, "messages");
  json_builder_begin_array (parser->builder);

  for (GList *cur = log->messages; cur != NULL; cur = cur->next) {
    ChatMessage *msg = cur->data;
    json_builder_begin_object (parser->builder);
    json_builder_set_member_name (parser->builder, "sender");
    json_builder_add_string_value (parser->builder, msg->sender);
    json_builder_set_member_name (parser->builder, "datetime");
    json_builder_add_string_value (parser->builder, msg->dt);
    json_builder_set_member_name (parser->builder, "content");
    json_builder_add_string_value (parser->builder, msg->message);
    json_builder_end_object (parser->builder);
  }

  json_builder_end_array (parser->builder);
  json_builder_end_object (parser->builder);
}

void
chat_log_print (ChatLog *this, GOutputStream *outs, GCancellable *can) {
  gsize _bytes_writ;
  GError *err = NULL;

  g_output_stream_printf (outs, &_bytes_writ, can, &err, "Chat name: [%s]\n", this->chat_name);
  if (err) {
    fprintf (stderr, "ERROR: %s\n", err->message);
  }

  for (GList *cur = this->messages; cur != NULL; cur = cur->next) {
    ChatMessage *msg = (ChatMessage *)cur->data;
    chat_message_print (msg, outs, can);
  }
}

void
chat_parser_print_json (ChatParser *parser) {
  JsonNode *root = NULL;
  JsonGenerator *gen = NULL;
  GError *err = NULL;

  root = json_builder_get_root (parser->builder);

  gen = json_generator_new();
  json_generator_set_root(gen, root);
  json_generator_to_stream (gen, G_OUTPUT_STREAM (parser->outs), parser->can, &err);
  if (err) {
    fprintf (stderr, "ERROR: %s\n", err->message);
  }

  g_object_unref(gen);
}

void
chat_parser_print (ChatParser *parser) {
  if (!parser->quiet) {
    if (parser->json) {
      chat_parser_print_json (parser);
    } else {
      for (GList *cur = parser->logs; cur != NULL; cur = cur->next) {
        ChatLog *log = cur->data;
        chat_log_print (log, parser->outs, parser->can);
      }
    }
  }
}

void
chat_parser_free (ChatParser *parser) {
  g_list_free_full (parser->logs, (GDestroyNotify)chat_log_free);

  if (parser->json) {
    g_object_unref (parser->builder);
  }

  if (parser->cur_date) {
    g_free (parser->cur_date);
  }

  if (parser->_ios) {
    g_object_unref (parser->_ios);
    /* if we are using a GIOStream, then our OutputStream belongs to it, and will be freed by it */
  } else {
    g_object_unref (parser->outs);
  }

  free (parser);
}

ChatParser *chat_parser_new (gboolean quiet, gboolean json, char *out_path, GCancellable *can) {
  GError *err = NULL;
  ChatParser *parser = g_malloc0 (sizeof (ChatParser));

  parser->quiet = quiet;
  parser->json = json;
  parser->cur_date = NULL;
  parser->logs = NULL;
  parser->builder = json ? json_builder_new () : NULL;
  parser->can = can; /* optional */

  /* prepar output stream */
  if (out_path) {
    parser->_ios = get_iostream_for_path (out_path, can);
    parser->outs = g_io_stream_get_output_stream (G_IO_STREAM (parser->_ios));

    if (err) {
      fprintf (stderr, "ERROR: %s\n", err->message);
      exit (1);
    }
  } else {
    parser->_ios = NULL;
    parser->outs = G_OUTPUT_STREAM (g_unix_output_stream_new (fileno (stdout), FALSE));
  }

  return parser;
}

ChatParser *chat_parser_new_for_options (int *argc, char ***argv, GCancellable *can) {
  GError *err = NULL;
  GOptionContext *context = NULL;
  ChatParser *parser = NULL;
  gboolean quiet = FALSE;
  gboolean json = FALSE;

  gchar *out_path = NULL;

  /* parse options */
  GOptionEntry entries[] = {
    { "quiet", 'q', 0, G_OPTION_ARG_NONE, &quiet, "Do not print parsed messages", NULL },
    { "json",  'j', 0, G_OPTION_ARG_NONE, &json,  "Print as JSON", NULL },
    { "output", 'o', 0, G_OPTION_ARG_FILENAME, &out_path, "File to print output to instead of stdout", "OUTFILE" },
    G_OPTION_ENTRY_NULL
  };

  context = g_option_context_new ("- parse amsn chat logs");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, argc, argv, &err)) {
    fprintf (stderr, "ERROR: %s\n", err->message);
    exit (1);
  }

  g_option_context_free (context);

  parser = chat_parser_new (quiet, json, out_path, can);

  return parser;
}

gchar *
read_field_upto_str (GDataInputStream *dins, const char *filename, gchar* stop_chars, GCancellable *can) {
  GString *gstr = NULL;
  GError *err = NULL;
  gulong stop_chars_len = strlen (stop_chars);
  char byte = 0;

  gstr = g_string_new ("");

  while (TRUE) {
    byte = g_data_input_stream_read_byte (dins, can, &err);

    if (err) {
      if (err->code == 0) {
        if (gstr->str[gstr->len-1] == '\n') {
          g_string_truncate (gstr, gstr->len - 1);
        }
        // EOS
      } else {
        fprintf (stderr, "ERROR: %s: possible end of file?: %d: %s\n", filename, err->code, err->message);
      }

      g_error_free (err);
      break;
    }

    g_string_append_c (gstr, byte);

    if (stop_chars_len <= gstr->len) {
      if (g_strcmp0 (&(gstr->str[gstr->len - stop_chars_len]), stop_chars) == 0) {
        g_string_truncate (gstr, gstr->len - stop_chars_len);
        /* reached stop */
        break;
      }
    }
  }

  return g_string_free_and_steal (gstr);
}

gchar *
read_field_upto_chars (GDataInputStream *dins, const char *filename, gchar* stop_chars, GCancellable *can) {
  gsize bytes_read;
  GError *err = NULL;
  gchar *field = NULL;

  if (!(field = g_data_input_stream_read_upto (dins, stop_chars, -1, &bytes_read, can, &err))) {
    if (err) {
      fprintf (stderr, "ERROR: %s: failed to read field: %s\n", filename, err->message);
      g_error_free (err);
    } // else, EOF
    return NULL;
  }

  return field;
}

gchar *
read_field_code (GInputStream *ins, const char *filename, gssize bytes_to_read, GCancellable *can) {
  GError *err = NULL;
  gchar *code = calloc (4, 1);
  gssize bytes_read = 0;

  if ((bytes_read = g_input_stream_read (ins, code, bytes_to_read, can, &err)) == -1) {
    fprintf (stderr, "ERROR: %s: failed to read 3 letter code.  %s\n", filename, err->message);
    free (code);
    return NULL;
  }

  if (bytes_read == 0) {
    free (code);
    return NULL; /* EOF */
  }

  return code;
}

gchar *
unix_time_to_iso8601_str (guint time) {
  GDateTime *dt = NULL;
  gchar *dt_str = NULL;

  dt = g_date_time_new_from_unix_local (time);
  dt_str = g_date_time_format (dt, "%Y-%m-%dT%H:%M:%S%:z");
  g_date_time_unref (dt);

  return dt_str;
}

gchar *
match_get (GRegex *re, gchar *orig_str, int match_num) {
  GMatchInfo *matches = NULL;
  gchar *match_str = NULL;

  if (g_regex_match (re, orig_str, G_REGEX_MATCH_DEFAULT, &matches)) {
    match_str = g_match_info_fetch (matches, match_num);
  }
  g_match_info_unref (matches); /* match_get() mostly serves to allow cleaner unrefing of matches while using if-else trees */

  return match_str;
}

gchar *
parse_date_field (gchar *orig_str, const char *filename) {
  gchar *time_str = NULL;
  gint64 time = 0;

  if ((time_str = match_get (g_unix_time_re, orig_str, 1))) {
    /* '|"TIME123456789' where 123456789 is seconds since epoch/unix time */
    gchar *end = NULL;

    time = g_ascii_strtoll (time_str, &end, 10);
    g_assert (time != 0);

    g_free (time_str);
    return unix_time_to_iso8601_str (time);
  } else if ((time_str = match_get (g_dmy_time_re, orig_str, 1))) {
    /* "d b Y H:M:S" from "Conversation started on 02 Jan 2006 02:05:34" records (2006) */
    struct tm tm = { 0 };
    const char *ret = NULL;

    ret = strptime (time_str, "%d %b %Y %H:%M:%S", &tm);

    if (ret) {
      g_free (time_str);
      time = mktime (&tm);

      return unix_time_to_iso8601_str (time);
    } else {
      fprintf (stderr, "WARNING: %s: date-time string in '%%d %%b %%Y %%H:%%M:%%S' format, but could not be parsed, possibly due to locale?  Using date as is: %s\n",
               filename, orig_str);

      return time_str;
    }
  } else if (g_regex_match (g_just_hms_time_re, orig_str, 0, NULL)) {
    /* "%H:%M:%S", time-only used for per-message timestamps in 2006 */
    return g_strdup (orig_str);
  } else {
    fprintf (stderr, "WARNING: %s: date-time string not in a recognized format: %s\n", filename, orig_str);
    return g_strdup (orig_str);
  }
}

Record
parse_record (ChatParser *parser, GDataInputStream *dins, const char *filename, GCancellable *can) {
  gchar *code = NULL;
  Record rec;

  code = read_field_code (G_INPUT_STREAM (dins), filename, 3, can);
  if (!code) {
    /* error or EOF */
    return (Record){ EOS, { NULL } };
  }

  if (g_strcmp0 (code, "RED") == 0) {
    gchar *start_exit_date = NULL;

    /* Conversation Start */
    g_free (read_field_upto_str (dins, filename, "[", can));
    if (parser->cur_date) {
      g_free (parser->cur_date);
      parser->cur_date = NULL;
    }
    start_exit_date = read_field_upto_str (dins, filename, "]", can);
    parser->cur_date = parse_date_field (start_exit_date, filename);
    g_free (start_exit_date);

    g_free (read_field_upto_str (dins, filename, FIELD_SEP, can));

    rec = (Record){ RED, { NULL } };
  } else if (g_strcmp0 (code, "GRA") == 0) {
    gchar *time_orig = NULL;
    gchar *time = NULL;
    gchar *user = NULL;
    gchar *message = NULL;
    ChatMessage *chat_msg = NULL;
    Record user_rec;
    Record message_rec;

    /* message timestmp */
    g_free (read_field_upto_str (dins, filename, "[", can));

    time_orig = read_field_upto_str (dins, filename, "] " FIELD_SEP, can);
    time = parse_date_field (time_orig, filename);
    g_free (time_orig);

    user_rec = parse_record (parser, dins, filename, can);
    g_assert (user_rec.type == ITA);
    user = user_rec.data.s;

    message_rec = parse_record (parser, dins, filename, can);
    g_assert (message_rec.type == C00);
    message = message_rec.data.c.message; /* NOTE: we're discarding code_suffix :) */
    free (message_rec.data.c.code_suffix);

    chat_msg = chat_message_new_take (user, parser->cur_date, time, message);
    free (time);

    rec = (Record){ CHA, .data.m = chat_msg };
  } else if (g_strcmp0 (code, "ITA") == 0) {
    /* user */
    gchar *user = read_field_upto_str (dins, filename, " :" FIELD_SEP, can);
    rec = (Record){ ITA, .data.s = user };
  } else if (g_strcmp0 (code, "C00") == 0) {
    /* message */
    gchar *message_code = NULL;
    gchar *message = NULL;

    message_code = read_field_code (G_INPUT_STREAM (dins), filename, 4, can);
    g_free (read_field_code (G_INPUT_STREAM (dins), filename, 1, can)); /* get rid of " " :D */
    message = read_field_upto_str (dins, filename, "\n" FIELD_SEP, can);

    rec = (Record){ C00, .data.c = { message, message_code } };
  } else {
    /* err: unknown code */
    fprintf (stderr, "ERROR: %s: unknown code '%s'\n", filename, code);
    rec = (Record){ ERR, { NULL } };
  }

  g_free (code);
  return rec;
}

GFile *
get_file_for_path (char *fpath, GCancellable *can) {
  GMatchInfo *matches = NULL;
  GFile *file = NULL;
  char *basename = NULL;

  file = g_file_new_for_path (fpath);
  basename = g_file_get_basename (file);

  if (!g_regex_match (g_filename_re, basename, G_REGEX_MATCH_DEFAULT, &matches)) {
    fprintf (stderr,
             "WARNING: file '%s' did not match filename format "
             "'<user>@<domain>.log'. Skipping.\n",
             fpath);
    g_object_unref (file);
    g_free (basename);
    return NULL;
  }
  g_free (basename);
  g_match_info_free (matches);

  if (!g_file_query_exists (file, can)) {
    fprintf (stderr, "ERROR: %s: file does not exist. Skipping.\n", fpath);
    g_object_unref (file);
    return NULL;
  }

  return file;
}

/**
 * conversed_amsn_parse_data:
 *
 * @dins: A GDataInputStream containing data from an AMSN chat log.
 * @filename: The const char* filename of the source of the AMSN chat log, for error reporting.
 * @can: Optional. A GCancellable to interrupt parsing as necessary, or NULL.
 *
 * Parses AMSN chat log data from a DataInputStream, returning a ChatLog.
 *
 * Returns: A ChatLog. Free with chat_log_free().
 */
ChatLog *
chat_parser_parse_data (ChatParser *parser, GDataInputStream *dins, const char *filename, GCancellable *can) {
  Record rec;
  ChatLog *log = NULL;

  log = chat_log_new (filename);

  /** line 1: conversation start date **/
  free (read_field_upto_str (dins, filename, FIELD_SEP, can));
  for (int i = 0; TRUE; i++) {
    if (!parser->quiet) {
      fprintf (stderr, "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b" "parsed %4d records", i);
    }

    rec = parse_record (parser, dins, filename, can);
    if (rec.type == CHA) {
      log->messages = g_list_prepend (log->messages, rec.data.m);
    }

    if (rec.type == EOS) {
      break;
    }
  }
  log->messages = g_list_reverse (log->messages);

  if (!parser->quiet) {
    fprintf (stderr, "\n");
  }

  if (parser->json) {
    chat_parser_add_log_to_json_builder (parser, log);
  }

  return log;
}

/**
 * conversed_amsn_parse_file:
 *
 * @fpath: The const char* file path of an AMSN chat log to parse.
 * @can: Optional. A GCancellable to interrupt parsing as necessary, or NULL.
 *
 * Parses AMSN chat log data for a given path, returning a ChatLog.
 *
 * Returns: A ChatLog. Free with chat_log_free().
 */
gboolean
chat_parser_parse_file (ChatParser* parser, char *fpath, GCancellable *can) {
  GFile *file = NULL;
  GError *err = NULL;
  GFileInputStream *fins = NULL;
  GDataInputStream *dins = NULL;
  char *filename = NULL;
  ChatLog *log = NULL;

  if (!(file = get_file_for_path (fpath, can))) {
    return FALSE;
  }

  filename = g_file_get_basename (file);

  g_debug ("Parsing %s...", filename);

  if (!(fins = g_file_read (file, can, &err)) || err) {
    fprintf (stderr, "ERROR: %s: failed to read file: %s\n", filename, err->message);

    g_error_free (err);
    g_free (filename);
    g_object_unref (file);
    return FALSE;
  }

  dins = g_data_input_stream_new (G_INPUT_STREAM (fins));

  log = chat_parser_parse_data (parser, dins, filename, can);

  parser->logs = g_list_prepend (parser->logs, log);

  g_object_unref (dins);
  g_object_unref (fins);
  g_free (filename);
  g_object_unref (file);

  return TRUE;
}

GRegex *
setup_regex (gchar *re_str) {
  GRegex *re;
  GError *err = NULL;

  if (!(re = g_regex_new (re_str, G_REGEX_DEFAULT, G_REGEX_MATCH_DEFAULT, &err)) || err) {
    fprintf (stderr, "ERROR: %s\n", err->message);
    exit (1);
  }

  return re;
}

#define HMS "[0-9]{2}:[0-9]{2}:[0-9]{2}"

void
free_regexes (void) {
  g_regex_unref (g_filename_re);
  g_regex_unref (g_unix_time_re);
  g_regex_unref (g_dmy_time_re);
  g_regex_unref (g_hms_time_re);
  g_regex_unref (g_just_hms_time_re);
  g_regex_unref (g_iso8061_dt_re);
}

void
setup_regexes (void) {
  /* TODO: add a mode where we don't rely on the filename, but instead derive a chat name from the names of participants */
  g_filename_re      = setup_regex ("^[^@]+@(.+)\\.log$");
  g_unix_time_re     = setup_regex ("\\|\"LTIME([0-9]+)");
  g_dmy_time_re      = setup_regex ("([0-9]{2} [A-Z][a-z]{2} [0-9]{4} " HMS ")");
  g_just_hms_time_re = setup_regex ("^(" HMS ")$");
  g_hms_time_re      = setup_regex ("(" HMS ")");
  g_iso8061_dt_re    = setup_regex ("([0-9]{4}\\-[0-9]{2}\\-[0-9]{2}T" HMS ")");
}

int
main (int argc, char *argv[]) {
  GCancellable *can = g_cancellable_new ();
  ChatParser *parser = NULL;

  setup_regexes ();

  parser = chat_parser_new_for_options (&argc, &argv, can);

  if (parser->json) {
    json_builder_begin_array (parser->builder);
  }

  for (int i = 1; i < argc; i++) {
    if (g_strcmp0 (argv[i], "--") == 0) {
      continue;
    }

    chat_parser_parse_file (parser, argv[i], can);
  }
  parser->logs = g_list_reverse (parser->logs);

  if (parser->json) {
    json_builder_end_array (parser->builder);
  }

  chat_parser_print (parser);

  chat_parser_free (parser);
  free_regexes ();
  g_object_unref (can);

  return 0;
}
