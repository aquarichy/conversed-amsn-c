# Conversed aMSN Parser

A parser for aMSN log files. aMSN, aka. Alvaro's Messenger, was an open source instant messenger for MSN/Windows Live.
See [the wikipedia article](https://en.wikipedia.org/wiki/AMSN). This parser is 
intended for use by [Conversed](https://gitlab.com/aquarichy/conversed). It was written in C
using [GLib](https://gitlab.gnome.org/GNOME/glib). It can export parsed logs into JSON.

## Build Dependencies

- glib2
- json-glib

## Building

``` shell
make 
```

To run valgrind to check for memory leaks, you can run 

``` shell
make check
```

## Running

``` shell
./amsn_parser [-j|-q|-o OUTFILE|-h]
```

- -j, --json: output as JSON
- -q, --quiet: no output (useful when just verifying that parsing will succeed, e.g. with Valgrind)
- -o, --output OUTFILE: specify a file other than stdout to print to
- -h, --help: print help.

If you want pretty-printed JSON, consider passing it to Python, like:

``` shell
./amsn_parser -j some.log | python -mjson.tool
```

E.g.

``` shell
[
    {
        "chat_name": "kim@pine.net.log",
        "messages": [
            {
                "sender": "Scott",
                "dt": "2006-01-02T00:24:54-08",
                "message": ":)"
            },
            {
                "sender": "Scott",
                "dt": "2006-01-02T00:25:23-08",
                "message": "Are you ready for diiiiinner?"
            },
            {
                "sender": "Kim",
                "dt": "2006-01-02T00:25:32-08",
                "message": "not really, as I was panicking"
            },
...
```

## Todo

If someone actually wants to use this as a library, there should be a little more work done.
The regexes could be incorporated into the parser, and more API exposed via the .h file. If you're interested,
message me and I'll add it.

## License

GPLv3+.

## Authors

Richard Schwarting <aquarichy@gmail.com> © 2023


