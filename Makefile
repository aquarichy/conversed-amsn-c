CC=gcc
CFLAGS=`pkg-config --cflags glib-2.0 gio-unix-2.0 json-glib-1.0` -pedantic -Wall -g
LDFLAGS=`pkg-config --libs glib-2.0 gio-2.0 gio-unix-2.0 json-glib-1.0`

all: compile_commands.json build/amsn_parser

check: .valgrind-passed

.valgrind-passed: build/amsn_parser glib.supp glib-g_file_new_for_path.supp
	valgrind --leak-check=full --error-exitcode=1 \
		--suppressions=glib.supp \
		--suppressions=glib-g_file_new_for_path.supp \
		-- build/amsn_parser -q test_data/* 2> valgrind.log
	echo "valgrind passed!";
	touch .valgrind-passed

glib.supp:
	curl "https://gitlab.gnome.org/GNOME/glib/-/raw/main/tools/glib.supp" -o "glib.supp"

build/amsn_parser: src/amsn_parser.c
	mkdir -p build
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $^

# Necessary for clangd, used by lsp-mode in emacs
compile_commands.json: Makefile
	make clean
	bear -- make build/amsn_parser

clean:
	rm -f compile_commands.json build/amsn_parser .valgrind-passed valgrind.log
	if [ -d build ]; then \
		rmdir build/; \
	fi
	find . -name "*~" -delete -or -name "#*#" -delete
